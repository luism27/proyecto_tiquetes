﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using BOL;
using Enteties;

namespace WindowsFormsApp1
{
    public partial class Reportes : Form
    {
        public Reportes()
        {
            ReportesBOL c = new ReportesBOL();
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            chart1.Titles.Add("Prueba");

           // chart1.Series["Rutas"].Points.AddXY("Enero", "1000");
            //chart1.Series["Rutas"].Points.AddXY("2", "150");
           
            unidadesVencidas();
            unidadesVigentes();
            cargarTerminales();
            salidasPanama();
            entradaCR();
            //montoPorTerminal();
            EncomiendasPorRutas();
            tiquetesRuta();
            //tiquetesRutaMas();
            cargarPlata();
            



        }
        /// <summary>
        /// Allows to charge the terminals with the The collected money
        /// </summary>
        public void cargarPlata()
        {
            ReportesBOL x = new ReportesBOL();
            Dictionary<string, double> d = x.books();
            foreach (KeyValuePair<string, double> item in d)
            {
                Series ser = chart9.Series.Add(item.Key);

                ser.Label = "#" + item.Value.ToString() + "\n" + item.Key;

                ser.Points.Add(item.Value);
               
            }

        }
        /// <summary>
        /// Allows to charge the terminals with the The collected money
        /// </summary>
        public void tiquetesRuta()
        {
            try
            {
                ReportesBOL x = new ReportesBOL();
                Dictionary<string, int> d = x.tiquetesRuta();

                int con = 1;
                foreach (KeyValuePair<string, int> item in d)
                {
                    Series ser = chart2.Series.Add(item.Key);

                    ser.Label = "#" + item.Value.ToString() + "\n" + item.Key;

                    ser.Points.Add(item.Value);
                    con++;
                }
            }
            catch (Exception be)
            {
                MessageBox.Show(be.Message);
            }

        }
        /// <summary>
        /// Allows to charge a chart with routes
        /// </summary>
        public void tiquetesRutaMas()
        {
            try
            {
                ReportesBOL x = new ReportesBOL();
                int mes = cbxMes.SelectedIndex+1;
                Dictionary<string, int> d = x.tiqxRut(mes);
                int con = 1;
                foreach (KeyValuePair<string, int> item in d)
                {
                    Series ser = chart3.Series.Add(item.Key);

                    ser.Label = "#" + item.Value.ToString() + "\n" + item.Key;

                    ser.Points.Add(item.Value);
                    con++;
                }
            }
            catch (Exception be)
            {
                MessageBox.Show(be.Message,"Hola");
            }

        }
        /// <summary>
        /// Allows to represent the routes which more and less sent packages
        /// </summary>
        public void EncomiendasPorRutas()
        {
            try
            {
                ReportesBOL x = new ReportesBOL();
                Dictionary<string, int> d = x.rutasEncomiendas();

                int con = 1;
                foreach (KeyValuePair<string, int> item in d)
                {
                    Series ser = chart1.Series.Add(item.Key + Convert.ToString(con));

                    ser.Label = "#" + item.Value.ToString() + "\n" + item.Key;

                    ser.Points.Add(item.Value);
                    con++;
                }
            }
            catch (Exception be)
            {
                MessageBox.Show(be.Message);
            }
        }

        /// <summary>
        /// Allows to charge a chart with differents dates
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
      
        /// <summary>
        /// Allows to charge the terminals
        /// </summary>
        public void cargarTerminales()
        {
          
        }
        /// <summary>
        /// Allows to generate a chart with the amount of money by each terminal
        /// </summary>
        public void montoPorTerminal()
        {

            ReportesBOL x = new ReportesBOL();
            string[] seri = { "San Jose", "Quepos", "Perez Zeledon", "Paso Canoas", "Panama" };
            string f = DateTime.Now.ToString("dd/MM/yyyy");
            double[] money = x.montoPorTerminal(f);
            
            for (int i = 0; i < seri.Length; i++)
            {
                Series ser = chart9.Series.Add(seri[i]);
                ser.Label = money[i].ToString();
                ser.Points.Add(money[i]);
            }
        }

        private void Reportes_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Allows to generate the chart that represents the month with more enters to Costa Rica
        /// </summary>
        public void entradaCR()
        {
            ReportesBOL v = new ReportesBOL();
            int[] meses = v.entradaCR();
            string[] seri = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
            for (int i = 0; i < seri.Length; i++)
            {
                Series ser = chart7.Series.Add(seri[i]);
                ser.Label = meses[i].ToString();
                ser.Points.Add(meses[i]);
            }
        }
        /// <summary>
        /// Allows to generate the chart that represents the month with more exits to Panama
        /// </summary>
        public void salidasPanama()
        {
            ReportesBOL v = new ReportesBOL();
            int[] meses = v.salidasPanama();
            string[] seri = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
            for (int i = 0; i < seri.Length; i++)
            {
                Series ser = chart6.Series.Add(seri[i]);
                ser.Label = meses[i].ToString();
                ser.Points.Add(meses[i]);
            }

        }
        private void TabPage4_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Allows to charge charts with different dates
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void DtDat_ValueChanged(object sender, EventArgs e)
        //{
        //    chart9.Series.Clear();
        //    ReportesBOL x = new ReportesBOL();
        //    string dat = dtDate.Value.ToString("dd/MM/yyyy");
        //    string[] seri = { "San Jose", "Quepos", "Perez Zeledon", "Paso Canoas", "Panama" };
        //    double[] money = x.montoPorTerminal(dat);
        //    for (int i = 0; i < seri.Length; i++)
        //    {
        //        Series ser = chart9.Series.Add(seri[i]);
        //        ser.Label = money[i].ToString();
        //        ser.Points.Add(money[i]);
        //    }
        //}
    




    /// <summary>
    /// Allows to charge the amount of money 
    /// </summary>
    public void ventaPorDia() {
            ReportesBOL v = new ReportesBOL();
           // List<TiqueteNacional> x = v.ventaPorDia();
        }
        /// <summary>
        /// Allows to show the buses without available permission
        /// </summary>
        public void unidadesVencidas()
        {
            ReportesBOL v = new ReportesBOL();
            List<Unidad> x = v.UnidadesSinPermiso();
            
            foreach (Unidad a in x)
            {
                chart4.Series["Vencidos"].Points.AddXY(a.Codigo, "150");
            }
        }
        /// <summary>
        /// Allows to show the buses with available permission
        /// </summary>
        public void unidadesVigentes()
        {
            ReportesBOL v = new ReportesBOL();
            List<Unidad> x = v.UnidadesConPermiso();
            foreach (Unidad a in x)
            {
                chart5.Series["Activos"].Points.AddXY(a.Codigo, "150");
            }
        }
        private void TabPage2_Click(object sender, EventArgs e)
        {

        }

        private void TabPage5_Click(object sender, EventArgs e)
        {

        }

        private void Chart2_Click(object sender, EventArgs e)
        {

        }

        private void Chart4_Click(object sender, EventArgs e)
        {

        }

        private void Chart1_Click(object sender, EventArgs e)
        {

        }

        private void Label4_Click(object sender, EventArgs e)
        {

        }

       
        /// <summary>
        /// Allows to select the date to charge the reservations for a specific date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DtFecha_ValueChanged(object sender, EventArgs e)
        {
            try
            {
               
                    chart8.Series.Clear();
                    ReportesBOL x = new ReportesBOL();

                    string fec = dtFecha.Value.ToString("dd/MM/yyyy");
                    Dictionary<string, int> ds = x.SaberBooks(fec);
                    foreach (KeyValuePair<string, int> item in ds)
                    {
                        Series ser = chart8.Series.Add(item.Key);
                        ser.Label = item.Value.ToString();
                        ser.Points.Add(item.Value);
                    }
                    
                
            }
            catch (Exception be)
            {
                MessageBox.Show(be.Message);
            }
        }
        /// <summary>
        /// Allows to charge the route with most sales
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CbxMes_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                chart3.Series.Clear();
                ReportesBOL x = new ReportesBOL();
                int mes = cbxMes.SelectedIndex + 1;
                Dictionary<string, int> d = x.tiqxRut(mes);
                int con = 1;
                foreach (KeyValuePair<string, int> item in d)
                {
                    Series ser = chart3.Series.Add(item.Key);

                    ser.Label = "#" + item.Value.ToString() + "\n" + item.Key;

                    ser.Points.Add(item.Value);
                    con++;
                }
            }
            catch (Exception be)
            {
                MessageBox.Show(be.Message, "Hola");
            }

        }
    }
}
