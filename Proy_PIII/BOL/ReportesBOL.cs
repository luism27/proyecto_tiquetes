﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enteties;
using DAL;

namespace BOL
{
    public class ReportesBOL
    {
        /// <summary>
        ///  Allows to  generate a LIST of Unidades without permission to travel
        /// </summary>
        /// <returns>LIST of Unidades without permission to travel</returns>
        public List<Unidad> UnidadesSinPermiso()
        {
            List<Unidad> ze = new List<Unidad>();
            UnidadDAL x = new UnidadDAL();
            List<Unidad> z = x.cargarUnidades();
            foreach (Unidad a in z)
            {
                if (Convert.ToDateTime(a.GSFechaVigencia).Date < DateTime.Now.Date)
                {
                    ze.Add(a);
                }
            }
            return ze;

        }
        /// <summary>
        /// Allows to  generate a LIST of Unidades with permission to travel
        /// </summary>
        /// <returns>LIST of Unidades with permission to travel</returns>
        public List<Unidad> UnidadesConPermiso()
        {
            List<Unidad> ze = new List<Unidad>();
            UnidadDAL x = new UnidadDAL();
            List<Unidad> z = x.cargarUnidades();
            foreach (Unidad a in z)
            {
                if (Convert.ToDateTime(a.GSFechaVigencia).Date > DateTime.Now.Date)
                {
                    ze.Add(a);
                }
            }
            return ze;
        }
        /// <summary>
        /// Allows to charge the reservations for each terminal
        /// </summary>
        /// <returns>Dictionary<string, double> with terminal name and amount of reservations</returns>
        public Dictionary<string, int> tiquetesRuta()
        {
            ReportesDAL x = new ReportesDAL();
            Dictionary<string, int> d = x.tiquetesRuta();
            return d;
        }
        /// <summary>
        /// Allows to charge the reservations for each terminal
        /// </summary>
        /// <returns>Dictionary<string, double> with terminal name and amount of reservations</returns>
        public Dictionary<string, int> tiquetesRutaMas()
        {
            int mayor = 0;
            string ma = "";
            ReportesDAL x = new ReportesDAL();
            Dictionary<string, int> d = x.tiquetesRuta();
            foreach (KeyValuePair<string, int> item in d)
            {

                if (mayor < item.Value)
                {
                    mayor = item.Value;
                    ma = item.Key;
                }

            }
            return d;
        }

        /// <summary>
        /// Allows to charge the reservations for each terminal
        /// </summary>
        /// <returns>Dictionary<string, double> with terminal name and amount of reservations</returns>
        public Dictionary<string, int> rutasEncomiendas()
        {
            int mayor = 0;
            string ma = "";
            int menor = 0;
            string me = "Ninguno";
            ReportesDAL x = new ReportesDAL();
            Dictionary<string, int> de = new Dictionary<string, int>();
            Dictionary<string, int> d = x.rutasEncomiendas();
            foreach (KeyValuePair<string, int> item in d)
            {

                if (mayor < item.Value)
                {
                    mayor = item.Value;
                    ma = item.Key;
                }

            }
            de.Add(ma, mayor);
            foreach (KeyValuePair<string, int> item in d)
            {

                if (item.Value < mayor)
                {
                    menor = item.Value;
                    me = item.Key;
                }
            }
            de.Add(me, menor);
            return de;
        }
        /// <summary>
        /// Allows to charge the reservations for each terminal
        /// </summary>
        /// <returns>Dictionary<string, double> with terminal name and amount of reservations</returns>
        public Dictionary<string, int> tiqxRut(int mes)
        {
            ReportesDAL x = new ReportesDAL();
            Dictionary<string, int> des = x.tiqxRut(mes);
            int mayor = 0;
            string ma = "";
            int menor = 0;
            string me = "Ninguno";
           
            Dictionary<string, int> de = new Dictionary<string, int>();
            
            foreach (KeyValuePair<string, int> item in des)
            {

                if (mayor < item.Value)
                {
                    mayor = item.Value;
                    ma = item.Key;
                }

            }
            de.Add(ma, mayor);
            return de;
        }

        /// <summary>
        /// Allows to get the total number of Departures to Panama
        /// </summary>
        /// <returns>total number of Departures to Panama</returns>
        public int[] salidasPanama()
        {
            ReportesDAL x = new ReportesDAL();
            int[] d = x.salidasPanama();
            return d;
        }
        /// <summary>
        /// Allows to get the total number of enters to Costa Rica
        /// </summary>
        /// <returns>total number of  enters to Costa Rica</returns>
        public int[] entradaCR()
        {
            ReportesDAL x = new ReportesDAL();
            int[] d = x.entradaCR();
            return d;
        }
        /// <summary>
        /// Allows to know the amount of reservations for a specific date and terminal
        /// </summary>
        /// <param name="ter">name of the terminal</param>
        /// <param name="fec">specific date</param>
        /// <returns>number of reservations</returns>
        public int reservas(string ter, string fec)
        {
            ReportesDAL x = new ReportesDAL();
            int d = x.reservas(ter, fec) + x.reservasNac(ter, fec);
            return d;
        }
        /// <summary>
        /// Allows to get the amount of money recolected by each terminal
        /// </summary>
        /// <param name="fecha">Specific date</param>
        /// <returns>mount of money recolected by each terminal</returns>
        public double[] montoPorTerminal(String fecha)
        {
            ReportesDAL x = new ReportesDAL();
            double[] d = x.montoNacional(fecha);
            double[] c = x.montoInternacional(fecha);
            for (int i = 0; i < d.Length; i++)
            {
                d[i] += c[i];

            }
            return d;
        }
        /// <summary>
        /// Allows to charge the reservations for each terminal
        /// </summary>
        /// <returns>Dictionary<string, double> with terminal name and amount of reservations</returns>
        public Dictionary<string, double> books()
        {
            ReportesDAL x = new ReportesDAL();
            Dictionary<string, double> d = x.books();
            return d;
        }
        /// <summary>
        /// Allows to charge the reservations for each terminal
        /// </summary>
        /// <param name="fec">Specific date</param>
        /// <returns>Dictionary<string, double> with terminal name and amount of reservations</returns>
        public Dictionary<string, int> SaberBooks(string fec)
        {
            ReportesDAL x = new ReportesDAL();
            Dictionary<string, int> d = x.SaberBooks(fec);
            return d;
        }
    }

}

