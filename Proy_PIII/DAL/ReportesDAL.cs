﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enteties;

namespace DAL
{
    public class ReportesDAL
    {
        /// <summary>
        /// Allows to get the total number of Departures to Panama
        /// </summary>
        /// <returns>total number of Departures to Panama</returns>
        public int[] salidasPanama()
        {
            int[] mes = new int[13];
            StreamReader lectura;
            StreamWriter escribir;
            string cadena, empleado;
            bool encontrado;
            encontrado = false;
            string[] campos = new string[13];
            char[] separador = { ',' };
            try
            {
                //lectura = File.OpenText(@"C:\Users\Usuario\Desktop\lol.txt");
                string path = Path.GetFullPath("tiqInter.txt");//para agregar carpetas afuera agrego ..\\
                lectura = File.OpenText(path);
                //escribir = File.CreateText(@"C:\Users\Usuario\Desktop\temp.txt");
                // String Nombre = dataTabla.CurrentRow.Cells[0].Value.ToString();
                cadena = lectura.ReadLine();
                while (cadena != null)
                {

                    campos = cadena.Split(separador);
                    if (campos[7].Equals("PNM001"))
                    {
                        DateTime x = Convert.ToDateTime(campos[2]);
                        int xx = x.Month - 1;
                        mes[xx] += 1;
                    }


                    /*
                    else
                    {
                        escribir.WriteLine(cadena);
                    }
                    */
                    cadena = lectura.ReadLine();
                }

                lectura.Close();
                //escribir.Close();
                /*
                File.AppendAllText(@"C:\Users\Usuario\Desktop\temp.txt", "Your Text" + "\n");
                File.Delete(@"C:\Users\Usuario\Desktop\lol.txt");
                File.Move(@"C:\Users\Usuario\Desktop\temp.txt", @"C:\Users\Usuario\Desktop\lol.txt");
                */
                return mes;
            }
            catch (FileNotFoundException fe)
            {

            }
            catch (Exception be)
            {

            }
            return mes;
        }
        /// <summary>
        /// Allows to get the total number of Departures to Panama
        /// </summary>
        /// <returns>total number of Departures to diferents terminals</returns>
        public Dictionary<string, int> tiquetesRuta()
        {
            Dictionary<string, int> resul = new Dictionary<string, int>();
            List<String> d = totalRutasNac();
            List<String> de = totalRutasInt();
            foreach(string dos in de)
            {
                d.Add(dos);
            }
            foreach (string item in d)
            {
                if (resul.ContainsKey(item))
                {
                    resul[item]++;
                }
                else
                {
                    resul.Add(item, 1);

                }
            }
            return resul;
        }
        /// <summary>
        /// Allows to get the total number of Departures to Panama
        /// </summary>
        /// <returns>total number of Departures to Panama</returns>
        private List<string> totalRutasInt()
        {
            List<string> busr = new List<string>();
            StreamReader lectura;
            StreamWriter escribir;
            string cadena, empleado;
            bool encontrado;
            encontrado = false;
            string[] campos = new string[15];
            char[] separador = { ',' };
            try
            {
                //lectura = File.OpenText(@"C:\Users\Usuario\Desktop\lol.txt");
                string path = Path.GetFullPath("tiqInter.txt");//para agregar carpetas afuera agrego ..\\
                lectura = File.OpenText(path);
                //escribir = File.CreateText(@"C:\Users\Usuario\Desktop\temp.txt");
                // String Nombre = dataTabla.CurrentRow.Cells[0].Value.ToString();
                cadena = lectura.ReadLine();
                while (cadena != null)
                {
                    campos = cadena.Split(separador);
                    if (campos[2].Equals(DateTime.Now.ToString("dd/MM/yyyy")))
                    {
                        busr.Add(nombreRuta(campos[4]));
                    }

                    /*
                    else
                    {
                        escribir.WriteLine(cadena);
                    }
                    */
                    cadena = lectura.ReadLine();
                }

                lectura.Close();
                //escribir.Close();
                /*
                File.AppendAllText(@"C:\Users\Usuario\Desktop\temp.txt", "Your Text" + "\n");
                File.Delete(@"C:\Users\Usuario\Desktop\lol.txt");
                File.Move(@"C:\Users\Usuario\Desktop\temp.txt", @"C:\Users\Usuario\Desktop\lol.txt");
                */
                return busr;
            }
            catch (FileNotFoundException fe)
            {

            }
            catch (Exception be)
            {

            }
            return busr;
        }
        /// <summary>
        /// Allows to charge the terminal with more tickets selled by months
        /// </summary>
        /// <param name="mes">month of the year</param>
        /// <returns>Dictionary<string, int> terminal with more tickets selled</returns>
        public Dictionary<string, int> tiqxRut(int mes)
        {
            Dictionary<string, int> de = new Dictionary<string, int>();
            Dictionary<string, int> d = tiqxRutNac(mes);
            Dictionary<string, int> resul = tiqxRutInter(mes);

            foreach (KeyValuePair<string, int> item in d)
            {
                if (de.ContainsKey(item.Key))
                {
                    de[item.Key] ++;
                }
                else
                {
                    de.Add(item.Key, 1);

                }
            }
            foreach (KeyValuePair<string, int> item in resul)
            {
                if (de.ContainsKey(item.Key))
                {
                    de[item.Key] += item.Value;
                }
                else
                {
                    de.Add(item.Key,1);

                }
            }
            return de;
        }
        /// <summary>
        /// Allows to charge the terminal with more tickets selled by months
        /// </summary>
        /// <param name="mes">month of the year</param>
        /// <returns>Dictionary<string, int> terminal with more tickets selled</returns>
        private Dictionary<string, int> tiqxRutInter(int mes)
        {
            Dictionary<string, int> busr = new Dictionary<string, int>();
            StreamReader lectura;
            StreamWriter escribir;
            string cadena, empleado;
            bool encontrado;
            encontrado = false;
            string[] campos = new string[15];
            char[] separador = { ',' };
            try
            {
                //lectura = File.OpenText(@"C:\Users\Usuario\Desktop\lol.txt");
                string path = Path.GetFullPath("tiqInter.txt");//para agregar carpetas afuera agrego ..\\
                lectura = File.OpenText(path);
                //escribir = File.CreateText(@"C:\Users\Usuario\Desktop\temp.txt");
                // String Nombre = dataTabla.CurrentRow.Cells[0].Value.ToString();
                cadena = lectura.ReadLine();
                while (cadena != null)
                {
                    campos = cadena.Split(separador);
                    DateTime n = Convert.ToDateTime(campos[2]);
                    if (n.Month == mes)
                    {
                        string r = nombreRuta(campos[4]);
                        if (busr.ContainsKey(r))
                        {
                            busr[r]++;
                        }
                        else
                        {
                            busr.Add(r, 1);

                        }

                    }

                    /*
                    else
                    {
                        escribir.WriteLine(cadena);
                    }
                    */
                    cadena = lectura.ReadLine();
                }

                lectura.Close();
                //escribir.Close();
                /*
                File.AppendAllText(@"C:\Users\Usuario\Desktop\temp.txt", "Your Text" + "\n");
                File.Delete(@"C:\Users\Usuario\Desktop\lol.txt");
                File.Move(@"C:\Users\Usuario\Desktop\temp.txt", @"C:\Users\Usuario\Desktop\lol.txt");
                */
                return busr;
            }
            catch (FileNotFoundException fe)
            {

            }
            catch (Exception be)
            {

            }
            return busr;
        }
        /// <summary>
        /// Allows to charge the terminal with more tickets selled by months
        /// </summary>
        /// <param name="mes">month of the year</param>
        /// <returns>Dictionary<string, int> terminal with more tickets selled</returns>
        private Dictionary<string, int> tiqxRutNac(int mes)
        {
            Dictionary<string, int> busr = new Dictionary<string, int>();
            StreamReader lectura;
            StreamWriter escribir;
            string cadena, empleado;
            bool encontrado;
            encontrado = false;
            string[] campos = new string[15];
            char[] separador = { ',' };
            try
            {
                //lectura = File.OpenText(@"C:\Users\Usuario\Desktop\lol.txt");
                string path = Path.GetFullPath("tiqNacional.txt");//para agregar carpetas afuera agrego ..\\
                lectura = File.OpenText(path);
                //escribir = File.CreateText(@"C:\Users\Usuario\Desktop\temp.txt");
                // String Nombre = dataTabla.CurrentRow.Cells[0].Value.ToString();
                cadena = lectura.ReadLine();
                while (cadena != null)
                {
                    campos = cadena.Split(separador);
                    DateTime n = Convert.ToDateTime(campos[1]);
                    if (n.Month==mes)
                    {
                        string r = nombreRuta(campos[11]);
                        if (busr.ContainsKey(r))
                        {
                            busr[r]++;
                        }
                        else
                        {
                            busr.Add(r, 1);

                        }
                        
                    }

                    /*
                    else
                    {
                        escribir.WriteLine(cadena);
                    }
                    */
                    cadena = lectura.ReadLine();
                }

                lectura.Close();
                //escribir.Close();
                /*
                File.AppendAllText(@"C:\Users\Usuario\Desktop\temp.txt", "Your Text" + "\n");
                File.Delete(@"C:\Users\Usuario\Desktop\lol.txt");
                File.Move(@"C:\Users\Usuario\Desktop\temp.txt", @"C:\Users\Usuario\Desktop\lol.txt");
                */
                return busr;
            }
            catch (FileNotFoundException fe)
            {

            }
            catch (Exception be)
            {

            }
            return busr;
        }
        /// <summary>
        /// Allows to now the name of a rout throug of a bus 
        /// </summary>
        /// <param name="bus">bus code</param>
        /// <returns>name of a rout</returns>
        public string nombreRuta(string bus)
        {
            string busr = "";
            StreamReader lectura;
            StreamWriter escribir;
            string cadena, empleado;
            bool encontrado;
            encontrado = false;
            string[] campos = new string[10];
            char[] separador = { ',' };
            try
            {
                //lectura = File.OpenText(@"C:\Users\Usuario\Desktop\lol.txt");
                string path = Path.GetFullPath("unidades.txt");//para agregar carpetas afuera agrego ..\\
                lectura = File.OpenText(path);
                //escribir = File.CreateText(@"C:\Users\Usuario\Desktop\temp.txt");
                // String Nombre = dataTabla.CurrentRow.Cells[0].Value.ToString();
                cadena = lectura.ReadLine();
                while (cadena != null)
                {
                    campos = cadena.Split(separador);
                    if (campos[0].Equals(bus))
                    {
                        busr = campos[6];
                    }

                    /*
                    else
                    {
                        escribir.WriteLine(cadena);
                    }
                    */
                    cadena = lectura.ReadLine();
                }

                lectura.Close();
                //escribir.Close();
                /*
                File.AppendAllText(@"C:\Users\Usuario\Desktop\temp.txt", "Your Text" + "\n");
                File.Delete(@"C:\Users\Usuario\Desktop\lol.txt");
                File.Move(@"C:\Users\Usuario\Desktop\temp.txt", @"C:\Users\Usuario\Desktop\lol.txt");
                */
                return busr;
            }
            catch (FileNotFoundException fe)
            {

            }
            catch (Exception be)
            {

            }
            return busr;
        }
        /// <summary>
        /// Allows to charge the terminal with more tickets selled by months
        /// </summary>
        /// <param name="fec">month of the year</param>
        /// <returns>Dictionary<string, int> terminal with more tickets selled</returns>
        public Dictionary<string, int> SaberBooks(string fec)
        {
            Dictionary<string, int> de = new Dictionary<string, int>();
            Dictionary<string, int> d = Internacional(fec);
            Dictionary<string, int> resul = Nacional(fec);

            foreach (KeyValuePair<string, int> item in d)
            {
                if (de.ContainsKey(item.Key))
                {
                    de[item.Key] += item.Value;
                }
                else
                {
                    de.Add(item.Key, item.Value);

                }
            }
            foreach (KeyValuePair<string, int> item in resul)
            {
                if (de.ContainsKey(item.Key))
                {
                    de[item.Key] += item.Value;
                }
                else
                {
                    de.Add(item.Key, item.Value);

                }
            }
            return de;
        }
        /// <summary>
        /// Allows to charge the terminal with more tickets selled by months
        /// </summary>
        /// <param name="fec">month of the year</param>
        /// <returns>Dictionary<string, int> terminal with more tickets selled</returns>
        private Dictionary<string, int> Nacional(string fec)
        {
            Dictionary<string, int> busr = new Dictionary<string, int>();
            StreamReader lectura;
            StreamWriter escribir;
            string cadena, empleado;
            bool encontrado;
            encontrado = false;
            string[] campos = new string[15];
            char[] separador = { ',' };
            try
            {
                //lectura = File.OpenText(@"C:\Users\Usuario\Desktop\lol.txt");
                string path = Path.GetFullPath("tiqNacional.txt");//para agregar carpetas afuera agrego ..\\
                lectura = File.OpenText(path);
                //escribir = File.CreateText(@"C:\Users\Usuario\Desktop\temp.txt");
                // String Nombre = dataTabla.CurrentRow.Cells[0].Value.ToString();
                cadena = lectura.ReadLine();
                while (cadena != null)
                {
                    campos = cadena.Split(separador);
                    if (campos[1].Equals(fec))
                    {

                        if (busr.ContainsKey(campos[5]))
                        {
                            busr[campos[5]]++;
                        }
                        else
                        {
                            busr.Add(campos[5], 1);

                        }

                    }

                    /*
                    else
                    {
                        escribir.WriteLine(cadena);
                    }
                    */
                    cadena = lectura.ReadLine();
                }

                lectura.Close();
                //escribir.Close();
                /*
                File.AppendAllText(@"C:\Users\Usuario\Desktop\temp.txt", "Your Text" + "\n");
                File.Delete(@"C:\Users\Usuario\Desktop\lol.txt");
                File.Move(@"C:\Users\Usuario\Desktop\temp.txt", @"C:\Users\Usuario\Desktop\lol.txt");
                */
                return busr;
            }
            catch (FileNotFoundException fe)
            {

            }
            catch (Exception be)
            {

            }
            return busr;
        }
        /// <summary>
        /// Allows to charge the terminal with more tickets selled by months
        /// </summary>
        /// <param name="fec">month of the year</param>
        /// <returns>Dictionary<string, int> terminal with more tickets selled</returns>
        private Dictionary<string, int> Internacional(string fec)
        {
            Dictionary<string, int> busr = new Dictionary<string, int>();
            StreamReader lectura;
            StreamWriter escribir;
            string cadena, empleado;
            bool encontrado;
            encontrado = false;
            string[] campos = new string[15];
            char[] separador = { ',' };
            try
            {
                //lectura = File.OpenText(@"C:\Users\Usuario\Desktop\lol.txt");
                string path = Path.GetFullPath("tiqInter.txt");//para agregar carpetas afuera agrego ..\\
                lectura = File.OpenText(path);
                //escribir = File.CreateText(@"C:\Users\Usuario\Desktop\temp.txt");
                // String Nombre = dataTabla.CurrentRow.Cells[0].Value.ToString();
                cadena = lectura.ReadLine();
                while (cadena != null)
                {
                    campos = cadena.Split(separador);
                    if (campos[2].Equals(fec))
                    {

                        if (busr.ContainsKey(campos[7]))
                        {
                            busr[campos[7]] ++;
                        }
                        else
                        {
                            busr.Add(campos[7], 1);

                        }

                    }

                    /*
                    else
                    {
                        escribir.WriteLine(cadena);
                    }
                    */
                    cadena = lectura.ReadLine();
                }

                lectura.Close();
                //escribir.Close();
                /*
                File.AppendAllText(@"C:\Users\Usuario\Desktop\temp.txt", "Your Text" + "\n");
                File.Delete(@"C:\Users\Usuario\Desktop\lol.txt");
                File.Move(@"C:\Users\Usuario\Desktop\temp.txt", @"C:\Users\Usuario\Desktop\lol.txt");
                */
                return busr;
            }
            catch (FileNotFoundException fe)
            {

            }
            catch (Exception be)
            {

            }
            return busr;
        }
        /// <summary>
        /// Allows to charge the terminal with more tickets selled by months
        /// </summary>
        /// <returns>Dictionary<string, int> terminal with more tickets selled</returns>
        public Dictionary<string, double> books()
        {
            Dictionary<string, double> de = new Dictionary<string, double>();
            Dictionary<string, double> d = totalTiqNac();
            Dictionary<string, double> resul = totalTiqInt();
            
            foreach (KeyValuePair<string, double> item in d)
            {
                if (de.ContainsKey(item.Key))
                {
                    de[item.Key]+=item.Value;
                }
                else
                {
                    de.Add(item.Key, item.Value);

                }
            }
            foreach (KeyValuePair<string, double> item in resul)
            {
                if (de.ContainsKey(item.Key))
                {
                    de[item.Key] += item.Value;
                }
                else
                {
                    de.Add(item.Key, item.Value);

                }
            }
            return de;
        } /// <summary>
          /// Allows to charge the terminal with more tickets selled by months
          /// </summary>
          /// <returns>Dictionary<string, int> terminal with more tickets selled</returns>
        private Dictionary<string, double> totalTiqInt()
        {
            Dictionary<string, double> busr = new Dictionary<string, double>();
            StreamReader lectura;
            StreamWriter escribir;
            string cadena, empleado;
            bool encontrado;
            encontrado = false;
            string[] campos = new string[15];
            char[] separador = { ',' };
            try
            {
                //lectura = File.OpenText(@"C:\Users\Usuario\Desktop\lol.txt");
                string path = Path.GetFullPath("tiqInter.txt");//para agregar carpetas afuera agrego ..\\
                lectura = File.OpenText(path);
                //escribir = File.CreateText(@"C:\Users\Usuario\Desktop\temp.txt");
                // String Nombre = dataTabla.CurrentRow.Cells[0].Value.ToString();
                cadena = lectura.ReadLine();
                while (cadena != null)
                {
                    campos = cadena.Split(separador);
                    if (campos[2].Equals(DateTime.Now.ToString("dd/MM/yyyy")))
                    {
                        
                            if (busr.ContainsKey(campos[6]))
                            {
                                busr[campos[6]] += Convert.ToDouble(campos[10]);
                            }
                            else
                            {
                                busr.Add(campos[6], Convert.ToDouble(campos[10]));

                            }
                        
                    }

                    /*
                    else
                    {
                        escribir.WriteLine(cadena);
                    }
                    */
                    cadena = lectura.ReadLine();
                }

                lectura.Close();
                //escribir.Close();
                /*
                File.AppendAllText(@"C:\Users\Usuario\Desktop\temp.txt", "Your Text" + "\n");
                File.Delete(@"C:\Users\Usuario\Desktop\lol.txt");
                File.Move(@"C:\Users\Usuario\Desktop\temp.txt", @"C:\Users\Usuario\Desktop\lol.txt");
                */
                return busr;
            }
            catch (FileNotFoundException fe)
            {

            }
            catch (Exception be)
            {

            }
            return busr;
        }
        /// <summary>
        /// Allows to charge the terminal with more tickets selled by months
        /// </summary>
        /// <returns>Dictionary<string, int> terminal with more tickets selled</returns>
        private Dictionary<string,double> totalTiqNac()
        {
            Dictionary<string, double> saz = new Dictionary<string, double>();
            List<string> busr = new List<string>();
            StreamReader lectura;
            StreamWriter escribir;
            string cadena, empleado;
            bool encontrado;
            encontrado = false;
            string[] campos = new string[15];
            char[] separador = { ',' };
            try
            {
                //lectura = File.OpenText(@"C:\Users\Usuario\Desktop\lol.txt");
                string path = Path.GetFullPath("tiqNacional.txt");//para agregar carpetas afuera agrego ..\\
                lectura = File.OpenText(path);
                //escribir = File.CreateText(@"C:\Users\Usuario\Desktop\temp.txt");
                // String Nombre = dataTabla.CurrentRow.Cells[0].Value.ToString();
                cadena = lectura.ReadLine();
                while (cadena != null)
                {
                    campos = cadena.Split(separador);
                    if (campos[1].Equals(DateTime.Now.ToString("dd/MM/yyyy")))
                    {
                        
                            if (saz.ContainsKey(campos[4]))
                            {
                                saz[campos[4]]+=Convert.ToDouble(campos[8]);
                            }
                            else
                            {
                                saz.Add(campos[4], Convert.ToDouble(campos[8]));

                            }
                        
                    }

                    /*
                    else
                    {
                        escribir.WriteLine(cadena);
                    }
                    */
                    cadena = lectura.ReadLine();
                }

                lectura.Close();
                //escribir.Close();
                /*
                File.AppendAllText(@"C:\Users\Usuario\Desktop\temp.txt", "Your Text" + "\n");
                File.Delete(@"C:\Users\Usuario\Desktop\lol.txt");
                File.Move(@"C:\Users\Usuario\Desktop\temp.txt", @"C:\Users\Usuario\Desktop\lol.txt");
                */
                return saz;
            }
            catch (FileNotFoundException fe)
            {

            }
            catch (Exception be)
            {

            }
            return saz;
        }

        private string nombreTerminal(string v)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Allows to charge the terminal with more tickets selled by months
        /// </summary>
        /// <returns>Dictionary<string, int> terminal with more tickets selled</returns>
        public Dictionary<string, int> rutasEncomiendas()
        {
            Dictionary<string, int> resul = new Dictionary<string, int>();
            List<String> d = totalRutas();
            foreach (string item in d)
            {
                if (resul.ContainsKey(item))
                {
                    resul[item]++;
                }
                else
                {
                    resul.Add(item, 1);

                }
            }
            return resul;

        }
        /// <summary>
        /// Allows to charge the terminal with more tickets selled by months
        /// </summary>
        /// <returns>Dictionary<string, int> terminal with more tickets selled</returns>
        public List<string> totalRutas()
        {
            List<string> busr = new List<string>();
            StreamReader lectura;
            StreamWriter escribir;
            string cadena, empleado;
            bool encontrado;
            encontrado = false;
            string[] campos = new string[10];
            char[] separador = { ',' };
            try
            {
                //lectura = File.OpenText(@"C:\Users\Usuario\Desktop\lol.txt");
                string path = Path.GetFullPath("Encomiendas.txt");//para agregar carpetas afuera agrego ..\\
                lectura = File.OpenText(path);
                //escribir = File.CreateText(@"C:\Users\Usuario\Desktop\temp.txt");
                // String Nombre = dataTabla.CurrentRow.Cells[0].Value.ToString();
                cadena = lectura.ReadLine();
                while (cadena != null)
                {
                    campos = cadena.Split(separador);
                    busr.Add(nombreRuta(campos[5]));

                    /*
                    else
                    {
                        escribir.WriteLine(cadena);
                    }
                    */
                    cadena = lectura.ReadLine();
                }

                lectura.Close();
                //escribir.Close();
                /*
                File.AppendAllText(@"C:\Users\Usuario\Desktop\temp.txt", "Your Text" + "\n");
                File.Delete(@"C:\Users\Usuario\Desktop\lol.txt");
                File.Move(@"C:\Users\Usuario\Desktop\temp.txt", @"C:\Users\Usuario\Desktop\lol.txt");
                */
                return busr;
            }
            catch (FileNotFoundException fe)
            {

            }
            catch (Exception be)
            {

            }
            return busr;

        }
        /// <summary>
        /// Allows to charge the terminal with more tickets selled by months
        /// </summary>
        /// <returns>Dictionary<string, int> terminal with more tickets selled</returns>
        public List<string> totalRutasNac()
        {
            List<string> busr = new List<string>();
            StreamReader lectura;
            StreamWriter escribir;
            string cadena, empleado;
            bool encontrado;
            encontrado = false;
            string[] campos = new string[15];
            char[] separador = { ',' };
            try
            {
                //lectura = File.OpenText(@"C:\Users\Usuario\Desktop\lol.txt");
                string path = Path.GetFullPath("tiqNacional.txt");//para agregar carpetas afuera agrego ..\\
                lectura = File.OpenText(path);
                //escribir = File.CreateText(@"C:\Users\Usuario\Desktop\temp.txt");
                // String Nombre = dataTabla.CurrentRow.Cells[0].Value.ToString();
                cadena = lectura.ReadLine();
                while (cadena != null)
                {
                    campos = cadena.Split(separador);
                    if (campos[1].Equals(DateTime.Now.ToString("dd/MM/yyyy")))
                    {
                        busr.Add(nombreRuta(campos[11]));
                    }

                    /*
                    else
                    {
                        escribir.WriteLine(cadena);
                    }
                    */
                    cadena = lectura.ReadLine();
                }

                lectura.Close();
                //escribir.Close();
                /*
                File.AppendAllText(@"C:\Users\Usuario\Desktop\temp.txt", "Your Text" + "\n");
                File.Delete(@"C:\Users\Usuario\Desktop\lol.txt");
                File.Move(@"C:\Users\Usuario\Desktop\temp.txt", @"C:\Users\Usuario\Desktop\lol.txt");
                */
                return busr;
            }
            catch (FileNotFoundException fe)
            {

            }
            catch (Exception be)
            {

            }
            return busr;

        }

        




        /// <summary>
        /// Allows to get the amount of money recolected by each terminal
        /// </summary>
        /// <param name="fecha">Specific date</param>
        /// <returns>mount of money recolected by each terminal</returns>
        public double[] montoInternacional(string fecha)
        {
            double[] cantidad = new double[5];
            StreamReader lectura;
            StreamWriter escribir;
            string cadena, empleado;
            bool encontrado;
            encontrado = false;
            string[] campos = new string[13];
            char[] separador = { ',' };
            try
            {
                //lectura = File.OpenText(@"C:\Users\Usuario\Desktop\lol.txt");
                string path = Path.GetFullPath("tiqInter.txt");//para agregar carpetas afuera agrego ..\\
                lectura = File.OpenText(path);
                //escribir = File.CreateText(@"C:\Users\Usuario\Desktop\temp.txt");
                // String Nombre = dataTabla.CurrentRow.Cells[0].Value.ToString();
                cadena = lectura.ReadLine();
                while (cadena != null)
                {
                    campos = cadena.Split(separador);
                    //if (campos[2].Equals(fecha))
                    //{
                    if (campos[6].Equals("SJ001"))
                    {
                        cantidad[0] += Convert.ToDouble(campos[10]);
                    }
                    else if (campos[6].Equals("QEP001"))
                    {
                        cantidad[1] += Convert.ToDouble(campos[10]);
                    }
                    else if (campos[6].Equals("PRZ001"))
                    {
                        cantidad[2] += Convert.ToDouble(campos[10]);
                    }
                    else if (campos[6].Equals("PC001"))
                    {
                        cantidad[3] += Convert.ToDouble(campos[10]);
                    }
                    else if (campos[6].Equals("PNM001"))
                    {
                        cantidad[4] += Convert.ToDouble(campos[10]);
                    }
                    //}



                    /*
                    else
                    {
                        escribir.WriteLine(cadena);
                    }
                    */
                    cadena = lectura.ReadLine();
                }

                lectura.Close();
                //escribir.Close();
                /*
                File.AppendAllText(@"C:\Users\Usuario\Desktop\temp.txt", "Your Text" + "\n");
                File.Delete(@"C:\Users\Usuario\Desktop\lol.txt");
                File.Move(@"C:\Users\Usuario\Desktop\temp.txt", @"C:\Users\Usuario\Desktop\lol.txt");
                */
                return cantidad;
            }
            catch (FileNotFoundException fe)
            {

            }
            catch (Exception be)
            {

            }
            return cantidad;
        }
        /// <summary>
        /// Allows to get the amount of money recolected by each terminal
        /// </summary>
        /// <param name="fecha">Specific date</param>
        /// <returns>amount of money recolected by each terminal</returns>
        public double[] montoNacional(string fecha)
        {
            double[] cantidad = new double[5];
            StreamReader lectura;
            StreamWriter escribir;
            string cadena, empleado;
            bool encontrado;
            encontrado = false;
            string[] campos = new string[13];
            char[] separador = { ',' };
            try
            {
                //lectura = File.OpenText(@"C:\Users\Usuario\Desktop\lol.txt");
                string path = Path.GetFullPath("tiqNacional.txt");//para agregar carpetas afuera agrego ..\\
                lectura = File.OpenText(path);
                //escribir = File.CreateText(@"C:\Users\Usuario\Desktop\temp.txt");
                // String Nombre = dataTabla.CurrentRow.Cells[0].Value.ToString();
                cadena = lectura.ReadLine();
                while (cadena != null)
                {
                    campos = cadena.Split(separador);
                    //if (campos[1].Equals(fecha))
                    //{
                    if (campos[4].Equals("SJ001"))
                    {
                        cantidad[0] += Convert.ToDouble(campos[8]);
                    }
                    else if (campos[4].Equals("QEP001"))
                    {
                        cantidad[1] += Convert.ToDouble(campos[8]);
                    }
                    else if (campos[4].Equals("PRZ001"))
                    {
                        cantidad[2] += Convert.ToDouble(campos[8]);
                    }
                    else if (campos[4].Equals("PC001"))
                    {
                        cantidad[3] += Convert.ToDouble(campos[8]);
                    }
                    else if (campos[4].Equals("PNM001"))
                    {
                        cantidad[4] += Convert.ToDouble(campos[8]);
                    }
                    //}



                    /*
                    else
                    {
                        escribir.WriteLine(cadena);
                    }
                    */
                    cadena = lectura.ReadLine();
                }

                lectura.Close();
                //escribir.Close();
                /*
                File.AppendAllText(@"C:\Users\Usuario\Desktop\temp.txt", "Your Text" + "\n");
                File.Delete(@"C:\Users\Usuario\Desktop\lol.txt");
                File.Move(@"C:\Users\Usuario\Desktop\temp.txt", @"C:\Users\Usuario\Desktop\lol.txt");
                */
                return cantidad;
            }
            catch (FileNotFoundException fe)
            {

            }
            catch (Exception be)
            {

            }
            return cantidad;
        }
        /// <summary>
        /// Allows to know the amount of reservations frr a specific date and terminal
        /// </summary>
        /// <param name="ter">name of the terminal</param>
        /// <param name="fec">specific date</param>
        /// <returns>number of reservations</returns>
        public int reservasNac(string ter, string fec)
        {
            int cantidad = 0;
            StreamReader lectura;
            StreamWriter escribir;
            string cadena, empleado;
            bool encontrado;
            encontrado = false;
            string[] campos = new string[13];
            char[] separador = { ',' };
            try
            {
                //lectura = File.OpenText(@"C:\Users\Usuario\Desktop\lol.txt");
                string path = Path.GetFullPath("tiqNacional.txt");//para agregar carpetas afuera agrego ..\\
                lectura = File.OpenText(path);
                //escribir = File.CreateText(@"C:\Users\Usuario\Desktop\temp.txt");
                // String Nombre = dataTabla.CurrentRow.Cells[0].Value.ToString();
                cadena = lectura.ReadLine();
                while (cadena != null)
                {
                    campos = cadena.Split(separador);

                    if (campos[1].Equals(fec) && campos[5].Equals(ter))
                    {
                        cantidad += 1;
                    }



                    /*
                    else
                    {
                        escribir.WriteLine(cadena);
                    }
                    */
                    cadena = lectura.ReadLine();
                }

                lectura.Close();
                //escribir.Close();
                /*
                File.AppendAllText(@"C:\Users\Usuario\Desktop\temp.txt", "Your Text" + "\n");
                File.Delete(@"C:\Users\Usuario\Desktop\lol.txt");
                File.Move(@"C:\Users\Usuario\Desktop\temp.txt", @"C:\Users\Usuario\Desktop\lol.txt");
                */
                return cantidad;
            }
            catch (FileNotFoundException fe)
            {

            }
            catch (Exception be)
            {

            }
            return cantidad;
        }
        /// <summary>
        /// Allows to know the amount of reservations frr a specific date and terminal
        /// </summary>
        /// <param name="ter">name of the terminal</param>
        /// <param name="fec">specific date</param>
        /// <returns>number of reservations</returns>
        public int reservas(string Ter, string fec)
        {
            int cantidad = 0;
            StreamReader lectura;
            StreamWriter escribir;
            string cadena, empleado;
            bool encontrado;
            encontrado = false;
            string[] campos = new string[13];
            char[] separador = { ',' };
            try
            {
                //lectura = File.OpenText(@"C:\Users\Usuario\Desktop\lol.txt");
                string path = Path.GetFullPath("tiqInter.txt");//para agregar carpetas afuera agrego ..\\
                lectura = File.OpenText(path);
                //escribir = File.CreateText(@"C:\Users\Usuario\Desktop\temp.txt");
                // String Nombre = dataTabla.CurrentRow.Cells[0].Value.ToString();
                cadena = lectura.ReadLine();
                while (cadena != null)
                {
                    campos = cadena.Split(separador);

                    if (campos[2].Equals(fec) && campos[7].Equals(Ter))
                    {
                        cantidad += 1;
                    }



                    /*
                    else
                    {
                        escribir.WriteLine(cadena);
                    }
                    */
                    cadena = lectura.ReadLine();
                }

                lectura.Close();
                //escribir.Close();
                /*
                File.AppendAllText(@"C:\Users\Usuario\Desktop\temp.txt", "Your Text" + "\n");
                File.Delete(@"C:\Users\Usuario\Desktop\lol.txt");
                File.Move(@"C:\Users\Usuario\Desktop\temp.txt", @"C:\Users\Usuario\Desktop\lol.txt");
                */
                return cantidad;
            }
            catch (FileNotFoundException fe)
            {

            }
            catch (Exception be)
            {

            }
            return cantidad;
        }
        /// <summary>
        /// Allows to get the total number of enters to Costa Rica
        /// </summary>
        /// <returns>total number of  enters to Costa Rica</returns>
        public int[] entradaCR()
        {
            int[] mes = new int[13];
            StreamReader lectura;
            StreamWriter escribir;
            string cadena, empleado;
            bool encontrado;
            encontrado = false;
            string[] campos = new string[15];
            char[] separador = { ',' };
            try
            {
                //lectura = File.OpenText(@"C:\Users\Usuario\Desktop\lol.txt");
                string path = Path.GetFullPath("tiqInter.txt");//para agregar carpetas afuera agrego ..\\
                lectura = File.OpenText(path);
                //escribir = File.CreateText(@"C:\Users\Usuario\Desktop\temp.txt");
                // String Nombre = dataTabla.CurrentRow.Cells[0].Value.ToString();
                cadena = lectura.ReadLine();
                while (cadena != null)
                {
                    campos = cadena.Split(separador);

                    if (!campos[7].Equals("PNM001"))
                    {
                        DateTime x = Convert.ToDateTime(campos[2]);
                        int xx = x.Month - 1;
                        mes[xx] += 1;
                    }



                    /*
                    else
                    {
                        escribir.WriteLine(cadena);
                    }
                    */
                    cadena = lectura.ReadLine();
                }

                lectura.Close();
                //escribir.Close();
                /*
                File.AppendAllText(@"C:\Users\Usuario\Desktop\temp.txt", "Your Text" + "\n");
                File.Delete(@"C:\Users\Usuario\Desktop\lol.txt");
                File.Move(@"C:\Users\Usuario\Desktop\temp.txt", @"C:\Users\Usuario\Desktop\lol.txt");
                */
                return mes;
            }
            catch (FileNotFoundException fe)
            {

            }
            catch (Exception be)
            {

            }
            return mes;
        }

    }
}
